"""Functionality for retrieving message data."""
from copy import deepcopy
from datetime import datetime
import itertools
import re

from cki_lib.gitlab import parse_gitlab_url

from .settings import DATAWAREHOUSE_URL
from .settings import LOGGER


def filter_tests(condition, tests):
    """Filter tests based on the condition."""
    return list(filter(condition, tests))


def get_unknown_issues(tests, status=None):
    """Get a list of unknown issues that need to be reported."""
    if status:
        tests = [t for t in tests if t.status == status]
    else:
        # Check both failures and errors, ignore other KCIDB statuses
        tests = [t for t in tests if t.status in ['FAIL', 'ERROR']]

    regressions = filter_tests(
        lambda t: any(occurrence.is_regression for occurrence in
                      t.issues_occurrences.list()),
        tests)
    unknown_issues = filter_tests(lambda t: not t.issues_occurrences.list(), tests)

    return regressions + unknown_issues


def get_gating_data(message_base, checkout):
    """Create messages for gating."""
    messages = []

    message_base['run']['url'] = f'{DATAWAREHOUSE_URL}/kcidb/checkouts/{checkout.misc["iid"]}'
    message_base['run']['log'] = message_base['run']['url']
    message_base['generated_at'] = datetime.utcnow().isoformat() + 'Z'
    message_base['artifact']['id'] = checkout.misc.get('brew_task_id')
    message_base['artifact']['scratch'] = checkout.misc.get('scratch', True)

    # 'contacts' is always a list with a single item for gating
    message_base['artifact']['issuer'] = checkout.attributes.get('contacts', ['CKI'])[0]

    # groupby requires sorted data based on the key
    sorted_builds = sorted(checkout.builds.list(), key=lambda b: b.architecture)
    for architecture, grouped_builds in itertools.groupby(sorted_builds,
                                                          key=lambda b: b.architecture):
        message = deepcopy(message_base)

        source_package_name = checkout.misc['source_package_name']
        message['artifact']['nvr'] = f'{source_package_name}-{checkout.misc["kernel_version"]}'

        message['test']['type'] = f'tier1-{architecture}'

        # Only add test results to complete test runs. We cannot rely on empty
        # test lists because of possible delays or super quick results!
        if 'result' in message['test']:
            unwaived_tests = filter_tests(
                lambda t: not t.waived,
                itertools.chain.from_iterable(build.tests.list() for build in grouped_builds)
            )
            failures = get_unknown_issues(unwaived_tests, 'FAIL')
            errors = get_unknown_issues(unwaived_tests, 'ERROR')

            # Failures are most important to report! If there are no failures nor
            # errors, everything passed.
            if failures:
                message['test']['result'] = 'failed'
            elif errors:
                message['test']['result'] = 'unknown'
                message['error'] = {'reason': 'Missing test results for a required test!'}
            else:
                message['test']['result'] = 'passed'

        message['pipeline']['id'] = f'{checkout.id}-{architecture}'
        message['artifact']['component'] = source_package_name
        message['system'][0]['architecture'] = architecture

        # Get all kpet tree families used in this summary
        kpet_families = {build.misc.get('kpet_tree_family', '') for build in grouped_builds}
        message['system'][0]['os'] = ','.join(kpet_families)

        messages.append(message)

    return messages


def get_ready_for_test_data(message, message_type, checkout):
    # pylint: disable=too-many-locals,too-many-statements
    """Add ready_for_test data to the message."""
    message['run']['url'] = f'{DATAWAREHOUSE_URL}/kcidb/checkouts/{checkout.misc["iid"]}'
    message['generated_at'] = datetime.utcnow().isoformat() + 'Z'
    message['branch'] = checkout.git_repository_branch or ''
    message['artifact']['issuer'] = checkout.attributes.get('contacts', ['CKI'])[0]
    message['artifact']['component'] = checkout.misc['source_package_name']

    if message_type == 'pre_test':
        message['cki_finished'] = False
        del message['status']  # Testing didn't finish yet
    else:
        message['cki_finished'] = True

    # Data specific to merge requests. Set to reasonable values and overwrite as needed.
    message['merge_request']['merge_request_url'] = ''
    message['merge_request']['is_draft'] = False
    message['merge_request']['subsystems'] = []
    message['merge_request']['bugzilla'] = []
    message['merge_request']['jira'] = []
    mr_data = checkout.misc.get('related_merge_request')
    if mr_data:
        message['merge_request']['merge_request_url'] = mr_data['url']
        _, mr_object = parse_gitlab_url(mr_data['url'])
        message['merge_request']['is_draft'] = mr_object.work_in_progress
        message['patch_urls'] = [mr_data['diff_url']]

        # Get BZ info, if provided
        message['merge_request']['bugzilla'] = re.findall(
            r'^Bugzilla:\s*(https?://[^\s]*)[\s]*$',
            mr_object.description,
            re.MULTILINE
        )

        # Get Jira info, if provided
        message['merge_request']['jira'] = re.findall(
            r'^JIRA:\s*(https?://[^\s]*)[\s]*$',
            mr_object.description,
            re.MULTILINE
        )

        # Get subsystem labels, if any
        message['merge_request']['subsystems'] = [
            label.split(':')[1] for label in mr_object.labels
            if label.startswith('Subsystem:')
        ]

    message['build_info'] = []
    unsupported_count = 0
    builds = checkout.builds.list()
    for build in builds:
        # skip if kpet decided that an arch should not be tested
        if build.misc.get('testing_skipped_reason') == 'unsupported':
            unsupported_count += 1
            continue

        debug_kernel = build.misc.get('debug', False)

        # build.output_files may or may not be available depending on whether
        # we have any other uploaded files; according to KCIDB schema empty
        # attributes get removed.
        output_files = build.output_files or {}
        kernel_file_data = next((output_file_data for output_file_data in output_files
                                 if output_file_data['name'] == 'kernel_package_url'), None)
        if not kernel_file_data:
            # The build failed, or something in the pipeline or data upload
            # went wrong. Either way, there is no build to test.
            continue

        partial_build_info = {
            'architecture': build.architecture,
            'build_id': build.id,
            'debug_kernel': debug_kernel,
            'kernel_package_url': kernel_file_data['url']
        }

        message['build_info'].append(partial_build_info)

    # QE would need to skip this incomplete checkout anyways, so let's return here.
    if not builds or len(message['build_info']) + unsupported_count != len(builds):
        LOGGER.warning('No data available for %s!', checkout.id)
        message['reason'] = 'No test data found!'
        return message

    message['modified_files'] = [file_data['path'] for file_data in
                                 checkout.misc.get('patchset_modified_files', [])]

    message['system'][0]['stream'] = builds[0].misc.get('kpet_tree_name', '')
    message['system'][0]['os'] = builds[0].misc.get('kpet_tree_family', '')
    message['artifact']['variant'] = builds[0].misc['package_name']

    if message_type != 'pre_test':
        # Calculate the summary across all runs.
        all_tests = checkout.all.get().tests
        unwaived_tests = filter_tests(lambda t: not t.waived, all_tests)
        failures = get_unknown_issues(unwaived_tests, 'FAIL')
        errors = get_unknown_issues(unwaived_tests, 'ERROR')

        # Failures are most important to report! If there are no failures nor
        # errors, everything passed.
        if failures:
            message['status'] = 'fail'
        elif errors:
            message['status'] = 'error'
        else:
            message['status'] = 'success'

    return message
