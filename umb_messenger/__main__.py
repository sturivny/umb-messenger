"""UMB messenge sender."""
import argparse
import sys

from cki_lib import metrics
from cki_lib import misc
import sentry_sdk

from . import settings
from . import umb


def process_message(umb_config=None, body=None, **_):
    """Filter and process messages if requested."""
    settings.LOGGER.info('Processing message for %s %s',
                         body['object_type'], body['id'])

    if body['object_type'] != 'checkout':
        settings.LOGGER.debug('Unsupported object type: %s', body['object_type'])
        return

    if misc.get_nested_key(body, 'object/misc/retrigger', False):
        settings.LOGGER.debug('Retriggered checkout, ignoring!')
        return

    # Return now so we save on requests if we don't care about the message
    if body['status'] not in ('ready_to_report', 'build_setups_finished'):
        settings.LOGGER.debug('Unsupported message: %s', body)
        return

    checkout = settings.DATAWAREHOUSE.kcidb.checkouts.get(id=body['id'])

    # Ignore checkouts that failed to merge. The data is valid, but there is
    # nothing to report.
    if not checkout.valid:
        return

    if body['status'] == 'build_setups_finished':
        if checkout.misc.get('brew_task_id') and not checkout.misc.get('scratch', True):
            umb.handle_message(umb_config, checkout, 'osci_running')
        if checkout.misc.get('send_ready_for_test_pre', False):
            umb.handle_message(umb_config, checkout, 'pre_test')
    elif body['status'] == 'ready_to_report':
        if checkout.misc.get('brew_task_id') and not checkout.misc.get('scratch', True):
            umb.handle_message(umb_config, checkout, 'osci_finished')
        if checkout.misc.get('send_ready_for_test_post', False):
            umb.handle_message(umb_config, checkout, 'post_test')


def main(args):
    """Run main loop."""
    umb_config = umb.load_configs()

    parser = argparse.ArgumentParser(description='Send UMB messages for CKI results')
    parser.add_argument('--queue', action='store_true',
                        help='Retrieve webhook data from an AMQP queue')
    parser.add_argument('--message-type',
                        help='Message type to send',
                        choices=['osci_running', 'osci_finished', 'pre_test', 'post_test'])
    parser.add_argument('--checkout-id',
                        help='Checkout ID to report')
    args = parser.parse_args(args)

    if args.message_type and args.checkout_id:
        checkout = settings.DATAWAREHOUSE.kcidb.checkouts.get(id=args.checkout_id)

        # This is a manual trigger, we don't need to sanity check anything as
        # the user *wants* to send the messages (otherwise they wouldn't be
        # running the hook).
        umb.handle_message(umb_config, checkout, args.message_type)
    elif args.queue:
        misc.sentry_init(sentry_sdk)
        metrics.prometheus_init()

        settings.QUEUE.consume_messages(
            settings.UMB_MESSENGER_EXCHANGE,
            ['#'],
            lambda **kwargs: process_message(umb_config=umb_config, **kwargs),
            queue_name=settings.UMB_MESSENGER_QUEUE,
        )
    else:
        settings.LOGGER.error('Either --queue or --checkout-id and --message-type needed')
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
