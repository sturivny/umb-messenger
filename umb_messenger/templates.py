"""Message templates."""
from copy import deepcopy

from cki_lib.config_tree import merge_dicts

# Message template for ready_for_test. "None" fields mark parts that are filled
# out based on actual pipeline data.
READY_FOR_TEST = {
    'ci': {
        'name': 'CKI (Continuous Kernel Integration)',
        'team': 'CKI',
        'docs': 'https://cki-project.org',
        'url': 'https://gitlab.com/cki-project',
        'irc': '#kernelci',
        'email': 'cki-project@redhat.com'
    },
    'run': {
        'url': None,
    },
    'artifact': {
        'type': 'cki-build',
        'issuer': None,
        'component': None,
    },
    'system': [{
        'os': None,
        'stream': None
    }],
    'build_info': None,
    'patch_urls': None,
    'merge_request': {
        'merge_request_url': None,
        'is_draft': None,
        'subsystems': None,
        'jira': None,
        'bugzilla': None
    },
    'branch': None,
    'modified_files': None,
    'cki_finished': None,
    'type': 'build',
    'category': 'kernel-build',
    'status': None,
    'namespace': 'cki',
    'generated_at': None,
    'version': '0.1.0'

}
PRE_TEST = READY_FOR_TEST
POST_TEST = READY_FOR_TEST

# Message templates for gating. "None" fields mark parts that are filled out
# based on actual pipeline data.
OSCI_COMMON = {
    'contact': {
        'name': 'CKI (Continuous Kernel Integration)',
        'team': 'CKI',
        'docs': 'https://cki-project.org',
        'url': 'https://gitlab.com/cki-project',
        'irc': '#kernelci',
        'email': 'cki-project@redhat.com',
        'environment': 'prod'
    },
    'run': {
        'url': None,
        'log': None,
    },
    'artifact': {
        'type': 'brew-build',
        'id': None,
        'issuer': None,
        'component': None,
        'variant': None,
        'nvr': None,
        'scratch': None
    },
    'system': [{
        'os': None,
        'provider': 'beaker',
        'architecture': None
    }],
    'pipeline': {
        'id': None,
        'name': 'cki-gating'
    },
    'test': {
        'type': None,
        'category': 'functional',
        'namespace': 'cki'
    },
    'generated_at': None,
    'version': '1.1.14'
}
OSCI_RUNNING = OSCI_COMMON
OSCI_FINISHED = merge_dicts(deepcopy(OSCI_COMMON), {'test': {'result': None}})
